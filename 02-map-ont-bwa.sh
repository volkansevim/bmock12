#!/bin/bash

FILENAME=ont.fastq

bwa mem -x ont2d -t 16\
	MOCK_12_REFS.fna \
	$FILENAME \
	> $FILENAME.sam \
	2>$FILENAME.log

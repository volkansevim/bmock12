#!/bin/bash

set -e
metaquast.py -s -t 12 -o ill_metaquast -R References/ ILL.out/scaffolds.fasta 
metaquast.py -s -t 12 -o ill_ont_metaquast -R References/ ONT_ILL.out/scaffolds.fasta 
metaquast.py -s -t 12 -o ill_pb_metaquast -R References/for_metaquast/ PB_ILL.out/scaffolds.fasta 

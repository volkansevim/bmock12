genomes=( 2615840527 2617270709 
	  2615840697 2616644829 
	  2615840601 2623620617 
	  2623620618 2615840533 
	  2615840646 2623620557 
	  2623620567 2623620609)
dirs=( ont_bwa pb_bwa ill_bwa )

for bamdir in ${dirs[@]}; do
  if [[ $bamdir == *"ill"* ]]; then
    PLATFORM="ILLUMINA"
  elif [[ $bamdir == *"ont"* ]]; then
    PLATFORM="ONT"
  else
    PLATFORM="PACBIO"
  fi
  
  for genome in "${genomes[@]}"; do
     echo $genome, $PLATFORM
     bedtools genomecov -ibam $bamdir/$genome.bam \
	    -g References/$genome.genome | \
	    fgrep genome | cut -f2,5 > \
	    GENOME_COV_"$PLATFORM"_$genome.TSV
  done
done



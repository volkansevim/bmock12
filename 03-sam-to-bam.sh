#!/bin/bash

i=$1
if [[ -z $i ]]; then
	echo "Need a sam name."
	exit
fi

samtools view -Sb $i | samtools sort -m 5G -@ 12 -o $i.bam -; 
samtools index $i.bam; 
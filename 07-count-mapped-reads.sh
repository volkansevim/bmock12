genomes=( 2615840527 2617270709 
	  2615840697 2616644829 
	  2615840601 2623620617 
	  2623620618 2615840533 
	  2615840646 2623620557 
	  2623620567 2623620609)

printf "TAXOID\tTOTAL_READS_MAPPED\n"
for genome in "${genomes[@]}"; do      
  printf "$genome\t"    
  samtools view -c $genome.bam
  
done


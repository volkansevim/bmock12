#!/bin/bash
set -e
# This script requires bbtools and jgi-mga that are freely available
#module load bbtools
#module load jgi-mga/2.0

ILLFASTQ=illumina.fastq
LIBBASE="illumina"
PBFASTQ=pacbio.fastq
ONTFASTQ=ont.fastq

reformat.sh  ow  in=$ILLFASTQ  out=$LIBBASE.reformat.fq.gz
bfc -1 -s 10g -k 21 -t 25  $LIBBASE.reformat.fq.gz | seqtk dropse - > $LIBBASE.reformat.fq.gz.bfc21.fastq

# illumina only
$SPADESPATH/spades.py -m 120 --tmp-dir $LSCRATCH -o ILL.out  \
    --only-assembler -k 33,55,77,99,127 --meta -t 14  \
    --12  $LIBBASE.reformat.fq.gz.bfc21.fastq

# illumina+pacbio hybrid  
$SPADESPATH/spades.py -m 140 --tmp-dir $LSCRATCH -o PB_ILL.out  \
     --only-assembler -k 33,55,77,99,127 --meta -t 20  \
     --12  $LIBBASE.reformat.fq.gz.bfc21.fastq --pacbio $PBFASTQ

# illumina+ont hybrid 
$SPADESPATH/spades.py -m 140 --tmp-dir $LSCRATCH -o ONT_ILL.out  \
     --only-assembler -k 33,55,77,99,127 --meta -t 20  \
     --12  $LIBBASE.reformat.fq.gz.bfc21.fastq --nanopore $ONTFASTQ


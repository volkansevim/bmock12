# This script requires picard and jgi_summarize_bam_contig_depths (available on bitbucket)

PICARDCMD='java -Xmx2g -jar picard.jar'
genomes=( 2615840527 2617270709 
	  2615840697 2616644829 
	  2615840601 2623620617 
	  2623620618 2615840533 
	  2615840646 2623620557 
	  2623620567 2623620609)
dirs=( ont_bwa pb_bwa ill_bwa )

for bamdir in ${dirs[@]}; do
  if [[ $bamdir == *"ill"* ]]; then
    PLATFORM="ILLUMINA"
  elif [[ $bamdir == *"ont"* ]]; then
    PLATFORM="ONT"
  else
    PLATFORM="PACBIO"
  fi
  PLATMAPPEDPATH=$bamdir
  echo
  echo $(basename $bamdir), $PLATFORM 
  echo
  
  for genome in "${genomes[@]}"; do      
    echo $genome "**"
    echo
      
    if [[ $PLATFORM == "ILLUMINA" ]]; then
      $PICARDCMD CollectInsertSizeMetrics \
	VERBOSITY=ERROR \
	I=$genome.bam \
	O=INSERT_SIZE_"$PLATFORM"_"$genome".out \
	H=INSERT_SIZE_"$PLATFORM"_"$genome".histbin
	tail -n+12 INSERT_SIZE_"$PLATFORM"_"$genome".out \
	> INSERT_SIZE_"$PLATFORM"_$genome.hist
    else
      samtools view $genome.bam |
	awk '{print length($10)}' | sort -n | uniq -c | \
	awk '{print $2"\t"$1;}' > INSERT_SIZE_$PLATFORM"_$genome".hist
    fi
    
    jgi_summarize_bam_contig_depths \
      --referenceFasta References/$genome.fa \
      --outputGC GC_PERCENT_"$PLATFORM"_"$genome".hist \
      "$genome.bam" \
      --percentIdentity 70
  done
done



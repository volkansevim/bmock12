#!/bin/bash
module load bbtools
set -e

ONTSS=ont.fastq
PB=pacbio.fastq
ILL=illumina.fastq
PREFIX=FASTQ_READ_LEN_DIST

readlength.sh in=$ONTSS out=$PREFIX"_ONT.TSV" bin=100 max=200000 nzo=f
readlength.sh in=$PB out=$PREFIX"_PACBIO.TSV" bin=100 max=200000 nzo=f
readlength.sh in=$SEQUEL out=$PREFIX"_SEQUEL.TSV" bin=100 max=200000 nzo=f

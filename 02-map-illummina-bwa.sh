#!/bin/bash
set -e
module load bwa/0.7.15

FILE=illumina.fastq

bwa mem -t 16 -p \
	MOCK_12_REFS.fna \
	$FILE \
        > $FILE.sam \
        2> logs/$FILE.log
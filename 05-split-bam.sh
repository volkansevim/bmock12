genomes=( 2615840527 2615840533 
	  2615840601 2615840646 
	  2615840697 2616644829 
	  2617270709 2623620557 
	  2623620567 2623620609 
	  2623620617 2623620618 )
bam=$1

if [[ -z $bam ]]; then
  echo "Need a bam name."
  exit
fi

if [[ $bam == *"ill"* ]]; then
  prm="-h -F 3844 -f 2"
else
  #these are not paired-end reads. don't use -f2
  prm="-h -F 3844"
fi

for genome in "${genomes[@]}"; do      
  if [ "$genome" -ne 2615840533 ]; then
    echo "Working on $genome"    
    samtools view $prm $bam $genome | \
	grep -P "@HD|@PG|$genome" | \
	samtools sort - -o $genome.bam -@ 20 -m 5G
  else
    echo "Working on 2615840533A and B"
    samtools view $prm $bam 2615840533A 2615840533B | \
	grep -P "@HD|@PG|$genome" | \
	samtools sort - -o $genome.bam -@ 20 -m 5G
  fi
  samtools index $genome.bam
done


# This script requires jgi_summarize_bam_contig_depths (available on bitbucket)

jgi_summarize_bam_contig_depths \
	--referenceFasta MOCK_12_REFS.fna \
	--outputReadStats READSTATS_ONT_MERGED.TSV \
	--outputGC GC_STATS_ONT.TSV ont_merged.bam

jgi_summarize_bam_contig_depths \
	--referenceFasta MOCK_12_REFS.fna \
	--outputReadStats READSTATS_PB_MERGED.TSV \
	--outputGC GC_STATS_PB.TSV pacbio_merged.bam




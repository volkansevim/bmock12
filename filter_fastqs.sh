#!/bin/bash -l
# pipeline to trim adapter, remove contamination (non-bacterial), error 
# correction and finaly assembly using meta spades.
#
set -e

module load pigz
module load spades/3.10.1
module load bbtools

THREADS=
MEMORY=

usage()
{
cat<<EOF

  Usage: $0 [options] <reads.fastq> <<reads.fastq>>
    <-t threads [32]> 
    <-m memory used in gigabytes [480]> 
    <-k starting kmer [33]> 

    defaults are shown in square brackets

EOF
exit 1
}


while getopts 't:m:' OPTION
do 
  case $OPTION in 
  m)    MEMORY="$OPTARG"
        ;;
  t)    THREADS="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

shift $((OPTIND - 1))
fastqName=("$@")

THREADS=${THREADS:=10}
MEMORY=${MEMORY:=50}

if [[ ! $fastqName ]]; then usage; fi

files=
# make sure we have full paths since
# I'm going to change working dirs
for f in ${fastqName[@]}; do
    if [[ ! -s $f ]]; then
        echo "Missing or empty file: $f"
        exit 1
    fi
    files+=$(realpath $f)
done


bname=`basename $fastqName .fastq.gz`
LOG="run.$bname.log"

echo -e "\n## check run.$bname.log for errors"

# filter junk sequences except microbes
echo -n "## running rqcfilter.sh ..."
echo "rqcfilter.sh threads=$THREADS overwrite in=$fastqName out=$bname.filt.fastq path=. library=frag trimfragadapter removehuman phix removemicrobes=f ow" >> $LOG
      rqcfilter.sh threads=$THREADS overwrite in=$fastqName out=$bname.filt.fastq path=. library=frag trimfragadapter removehuman phix removemicrobes=f ow 2>> $LOG
if [[ $? > 0 ]]; then echo "Failed rqcfilter.sh";exit 1; else echo "success"; fi


#!/bin/bash

FILENAME=pacbio.fastq
bwa mem -x pacbio -t 16\
	MOCK_12_REFS.fna \
	$FILENAME \
	>$FILENAME.sam \
	2>$FILENAME.log
